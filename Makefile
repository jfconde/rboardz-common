MAKEFILE_VERSION=0.0.1

install:
	@echo "Installing dependencies using NPM"
	npm install

build:
	@echo "Running build using @babel/cli"
	npm run build
	git checkout package-lock.json # I dont know why but package-lock.json is always modified.

publish:
	$(eval PKG_VER=$(shell sh -c "cat package.json | grep version | sed 's/^.*\"version\": \"\(.*\)\".*/\1/'"))
	$(eval VERSION=$(PKG_VER)-$(CI_COMMIT_SHORT_SHA))
	$(eval NPM_TAG=latest-unstable)
	npm version $(VERSION)
	@echo "Version to publish: $(VERSION)"
	@echo "Tag to publish: $(NPM_TAG)"
	@echo "Publishing package with tag: " << tag
	npm publish --tag $(NPM_TAG)
	@echo "Image published, version: $(VERSION)"

publish-master:
	npm version patch
	$(eval VERSION=$(shell sh -c "cat package.json | grep version | sed 's/^.*\"version\": \"\(.*\)\".*/\1/'"))
	$(eval NPM_TAG=next)
	@echo "Version to publish: $(VERSION)"
	@echo "Tag to publish: $(NPM_TAG)"
	@echo "Publishing package with tag: " << tag
	git push
	@echo "Git info pushed."
	npm publish --tag $(NPM_TAG)
	git push --tags
	@echo "Image published, version: $(VERSION)"
