import widgets from './widgets/';
import lib from './lib';

module.exports ={
    ...widgets,
    ...lib
};