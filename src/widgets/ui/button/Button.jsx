import React from 'react';
import prefix from '../prefix';
import PropTypes from 'prop-types';

const Button = (props = {}) => {
    const {
              text      = null,
              iconCls   = null,
              className = '',
              onClick   = null
          } = props;

    return (
        <div className={`${prefix}button ${className}`} onClick={onClick || null}>
            {iconCls && <i className={`${prefix}button_icon ${iconCls}`}/>}
            {text && <span className={`${prefix}button_text`}>{text}</span>}
        </div>
    );
};

Button.propTypes = {};

export default Button;