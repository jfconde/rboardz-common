import React from 'react';
import PropTypes from 'prop-types'
import BreadCrumbItem from './BreadCrumbItem';

const BreadCrumb = (props = {}) => {
    const {
        className = '',
        data = [],
        ui = ''
    } = props;

    return (
        <div className={`app-bread-crumb ${ui ? `app-bread-crumb--${ui}` : ''} ${className}`}>
            {
                data.map((item, i) =>
                    [
                        <BreadCrumbItem
                            data={item}
                            ui={ui}
                            key={i + item.route}
                        />,
                        <div className="app-bread-crumb-separator" key={`s${i}`}/>
                    ]
                )
            }
        </div>
    );
};

BreadCrumb.propTypes = {
    className: PropTypes.string,
    data     : PropTypes.arrayOf(PropTypes.shape({
        className: PropTypes.string,
        icon     : PropTypes.string,
        route    : PropTypes.string.isRequired,
        siblings : PropTypes.array,
        text     : PropTypes.string.isRequired,
        tooltip  : PropTypes.string,
        ui       : PropTypes.string
    })).isRequired,
    ui       : PropTypes.string,
};

export default BreadCrumb;