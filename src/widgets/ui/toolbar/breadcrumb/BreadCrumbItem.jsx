import React from 'react';
import PropTypes from 'prop-types';

const BreadCrumbItem = (props = {}) => {
    const {
        data: {
            text = '',
            icon = '',
            tooltip = '',
            className = '',
            route = '',
            siblings = [],
        } = {},
        ui = ''
    } = props;

    return (
        <div className={`app-bread-crumb-item ${ui ? `app-bread-crumb-item--${ui}` : ''} ${className}`}>
            {icon && <i className={`app-bread-crumb-item__icon fas fa-${icon}`}/>}
            {text && <span className="app-bread-crumb-item__text">{text}</span>}
        </div>
    );
};

BreadCrumbItem.propTypes = {
    data: PropTypes.shape({
        className: PropTypes.string,
        icon     : PropTypes.string,
        route    : PropTypes.string.isRequired,
        siblings : PropTypes.array,
        text     : PropTypes.string.isRequired,
        tooltip  : PropTypes.string,
        ui       : PropTypes.string
    }).isRequired
};

export default BreadCrumbItem;