import React, {useState} from 'react';
import SlideshowItem from './SlideshowItem';
import PropTypes from 'prop-types';
import {conditionalClassNames} from '../../../lib/util/classnames';

const baseClassName = 'slideshow';
const handleClassName = 'slideshow-handle';
const dotButtonContainerClassName = `${baseClassName}-dots`;
const dotButtonClassName = `${dotButtonContainerClassName}__dot`;
const dotButtonActiveClassName = 'active';

const getPrevIndex = (activeIndex, length) => activeIndex > 0 ? activeIndex-1 : length-1;
const getNextIndex = (activeIndex, length) => activeIndex < length - 1 ? activeIndex + 1 : 0;

const Handle = (className = '', onClick) => {
    return (
        <div className={`${handleClassName} ${className}`} onClick={onClick}>
        </div>
    );
};

const DotButtons = (count, activeSlideIndex, onClick) => {
    return (new Array(count)).fill(null).map((_, i) => (
        <span key={i} className={conditionalClassNames(dotButtonClassName, {
            [dotButtonActiveClassName]: i === activeSlideIndex
        })} onClick={() => onClick(i)} />
    ));
};

const Slideshow = props => {
    const {slides = [], height, ...xProps} = props;

    const [activeSlideIndex, setActiveSlideIndex] = useState(0);

    const prevIndex = getPrevIndex (activeSlideIndex, slides.length);
    const nextIndex = getNextIndex (activeSlideIndex, slides.length);

    const prevHandler = () => setActiveSlideIndex(prevIndex);
    const nextHandler = () => setActiveSlideIndex(nextIndex);
    const dotHandler = (index) => setActiveSlideIndex(index);

    return (
        <div className={baseClassName} {...xProps} style={{height}}>
            {Handle('prev', prevHandler)}
            {
                slides.map((slideData, slideIndex) => (
                    <SlideshowItem
                        key={slideIndex}
                        {...slideData}
                        active={slideIndex === activeSlideIndex}
                        isPrevious={slideIndex === prevIndex}
                        isNext={slideIndex === nextIndex}
                        height={height}
                    />
                ))
            }
            <span className={dotButtonContainerClassName}>
                {DotButtons(slides.length, activeSlideIndex, dotHandler)}
            </span>
            {Handle('next', nextHandler)}
        </div>
    );
};

Slideshow.defaultProps = {
    height: 420
};

Slideshow.propTypes = {
    slides: PropTypes.array,
    height: PropTypes.number
};

export default Slideshow;