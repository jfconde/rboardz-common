import React from 'react';
import PropTypes from 'prop-types';
import {conditionalClassNames} from '../../../lib/util/classnames';

const baseClassName = 'slideshow-item';
const activeClassName = `${baseClassName}-active`;
const previousClassName = `${baseClassName}-prev`;
const nextClassName = `${baseClassName}-next`;

const SlideshowItem = props => {
    const {
              src,
              title,
              subtitle,
              height,
              className,
              href,
              active     = false,
              isPrevious = false,
              isNext     = false
          } = props;

    const slideClassName = conditionalClassNames([baseClassName, className], {
        [activeClassName]  : active,
        [previousClassName]: isPrevious,
        [nextClassName]    : isNext
    });

    const slideStyle = {
        backgroundImage: `url(${src})`,
        height
    };

    return (
        <div className={slideClassName} style={slideStyle}>
            {
                href ? (
                    <>
                        <a href={href} className="slideshow-item__title">{title}</a>
                        <a href={href} className='slideshow-item__subtitle'>{subtitle}</a>
                    </>
                ) : (
                    <>
                        <span className="slideshow-item__title">{title}</span>
                        <span className="slideshow-item__subtitle">{subtitle}</span>
                    </>
                )
            }

        </div>
    );
};

SlideshowItem.propTypes = {
    src       : PropTypes.string.isRequired,
    title     : PropTypes.string,
    subtitle  : PropTypes.string,
    height    : PropTypes.number.isRequired,
    className : PropTypes.string,
    href      : PropTypes.string,
    active    : PropTypes.bool,
    isPrevious: PropTypes.bool,
    isNext    : PropTypes.bool
};

export default SlideshowItem;