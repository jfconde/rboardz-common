import React from 'react';
import PropTypes from 'prop-types';

const baseClassName = 'icon';

const Icon = (props) => {
    const { icon, className } = props;

    const classes = [baseClassName, className, `icon-${icon}`].filter(Boolean).join(' ');

    return (
        <span className={classes}>
        </span>
    );
};

Title.propTypes = {
    className: PropTypes.string,
    icon: PropTypes.string.isRequired
};

export default Icon;