import React from 'react';
import PropTypes from 'prop-types';

const baseClassName = 'title';

const Title = (props) => {

    return (
        <span className={baseClassName}>
            {props.children}
        </span>
    );
};

Title.propTypes = {
    children: PropTypes.array
};

export default Title;