import {call, put, all} from 'redux-saga';

const extractEndpointMethod = (api, endpointPath = '', endpointMethod = 'get') => {
    const endpoint = endpointPath.split('.').reduce((prev, k) => prev[k], api.endpoints);
    return endpoint[endpointMethod.toLowerCase()];
};

const sagaFn = function* (config, api) {
    const {
              successAction,
              errorAction,
              mapperIn,
              mapperOut,
              endpointPath,
              endpointMethod,
              requestConfig = {}
          } = config;

    const { data: _data } = requestConfig || {};
    let data = _data && (mapperIn ? mapperIn(_data) : _data);
    const method = extractEndpointMethod(api, endpointPath, endpointMethod);

    try {
        const response = yield call(() => method({...requestConfig, data}));
        yield put({
            type: successAction,
            payload: {
                data: response
            }
        });
    }
    catch (error) {
        yield put({
            type: errorAction,
            payload: {
                error
            }
        });
    }

};

export const httpApiRequestSaga = config => (action) => {
    // Requires middleware to inject api object.
    const { api } = action;
    return sagaFn(config, api);
};