import pickBy from '../util/pickBy';

export const handleActions = (actionsHash = {}, initialState = {}) => (state = initialState, action) =>
    Object.keys(actionsHash)
        .filter(k => Array.isArray(k) ? k.includes(action.type) : k === action.type)
        .reduce((curState, key) => actionsHash[key](curState, action, action.payload || {}) || console.warn(`handleActions: reducer returned null-sy value for ${action.type}.`) === -1 || curState, state);

export const withChildReducers = (reducer, childReducerHash) => (state, action) => {     const childKeys = Object.keys(childReducerHash);     return childKeys.reduce((curState, key) => ({         ...curState,         [key]: childReducerHash[key](state, action)     }), reducer(pickBy(state, k => !childKeys.includes(k))), action); };

export const actionGenerator = (type, prefix) => actionName => `${type}/${prefix}/${actionName}`.toUpperCase();

export const withPayload = (actionType, payload) => ({type: actionType, payload});
export const withoutPayload = (actionType) => ({type: actionType});
export const successAction = (originalAction) => `${originalAction}_OK`;
export const failureAction = (originalAction) => `${originalAction}_ERR`;
