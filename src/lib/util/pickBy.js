export const pickBy = (o, predicate) => {
    const res = {};
    Object.keys(o).filter(k => predicate(k, o[k])).forEach(k => res[k] = o[k]);
    return res;
};