export const unique = a => [...new Set(a)];
export const remove = (a, el) => a.filter(item => item !== el);