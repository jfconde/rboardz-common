/**
 * Generate a className string, given an Object containing classNames as keys and booleans as values
 * indicating if each className defined as key should be included.
 *
 * The baseClassName can be omitted for the sake of simplicity. In such case (no 2nd parameter), the 1st
 * parameter will be considered as the conditionalClassNames parameter.
 *
 * @param baseClassName A base className string that will always be applied, unconditionally.
 * @param conditionalClassNames An object containing classNames as keys and boolean as values.
 * @returns {string} The formed CSS className string.
 */
export const conditionalClassNames = function(baseClassName, conditionalClassNames) {
    if (!conditionalClassNames) {
        conditionalClassNames = baseClassName;
        baseClassName = '';
    }

    if(Array.isArray(baseClassName)) {
        baseClassName = baseClassName.filter(Boolean).join(' ');
    }

    const conditionalClassNamesString = Object.keys(conditionalClassNames)
        .map(className => conditionalClassNames[className] && className)
        .filter(Boolean)
        .join(' ');

    return `${baseClassName} ${conditionalClassNamesString}`;
};