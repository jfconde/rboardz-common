/** stdhead
 * App: rboardz/webapp
 * Author: Juan Carlos Fernández Conde <fconde.j@gmail.com>
 * Year: 2019
 */
export const openOAuthLogin = (config = {}) => {
    const {
        authorizeUri = '',
        redirectUri = ''
    } = config;

    return new Promise((resolve, reject) => {
        const popup = window.open(authorizeUri);
        if (!popup) {
            reject('POP_CREATION_FAILED');
            return;
        }

        popup.addEventListener('loginSuccess', () => alert('Success!'));
    });
};

