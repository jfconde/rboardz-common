import * as httpAPI from './httpAPI';
import * as redux from './redux';
import * as reduxSaga from './redux-saga';
import * as routes from './routes';
import * as util from './util';
import * as reduxIOMiddleware from './redux-io';
import { openOAuthLogin } from './oauth';

module.exports = {
    ...httpAPI,
    ...redux,
    ...reduxSaga,
    ...routes,
    ...util,
    ...reduxIOMiddleware,
    openOAuthLogin
};

delete module.exports.default;