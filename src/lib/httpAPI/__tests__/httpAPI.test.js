import HttpAPI from '../httpAPI';
import mockAxios from 'jest-mock-axios';


const modelEndpointsHash = {
    urlService   : 'urlService',
    objectService: {
        url : 'objectService',
        host: 'childHost',
        path: 'childPath'
    },
    parent1      : {
        parent2: {
            child: 'urlChild'
        }
    }
};

describe('lib/httpAPI', () => {
    it('Can be instantiated without arguments', () => expect(new HttpAPI()).toBeTruthy());
    it('Can be instantiated with null arguments', () => expect(new HttpAPI(null, null, null)).toBeTruthy());
    it('Will expose the given host and path arguments', () => {
        const api = new HttpAPI('h', 'p');
        expect(api.host).toBe('h');
        expect(api.path).toBe('p');
    });
    it('Will generate the right endpoint methods', () => {
        const api = new HttpAPI(null, null, modelEndpointsHash);
        expect(api.endpoints.urlService).toBeDefined();
        expect(api.endpoints.objectService).toBeDefined();
        expect(api.endpoints.parent1.parent2.child).toBeDefined();
        // Test the hardest
        const endpoint = api.endpoints.parent1.parent2.child;
        endpoint.get();
        expect(mockAxios.get).toHaveBeenCalled();
    });

});