const apiMiddleware = api => store => next => action => {
    action.httpApi = api;
    next(action);
};

export default apiMiddleware;