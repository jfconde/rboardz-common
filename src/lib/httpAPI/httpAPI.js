import axios from 'axios';
import {httpVerbs} from './definitions';

/**
 * We provide basic functionality to generate URLs and parse them according to a specific set of rules.
 */
class HttpAPI {

    host = null;
    path = null;

    constructor(host = '', path = '', services = {}) {
        this.host = host || '';
        this.path = path || '';
        this.endpoints = this.generateServicesObject(services || {});
    }

    generateServicesObject(services = {}) {
        return Object.keys(services).reduce(
            (resObj, serviceName) => ({
                ...resObj,
                [serviceName]: (typeof (services[serviceName]) === 'string' || !!services[serviceName].url) ?
                               this.createEndpointUtilityObject(services[serviceName]) :
                               this.generateServicesObject(services[serviceName])
            }), {}
        );
    }

    createEndpointUtilityObject(config = {}) {
        const url = typeof (config) === 'string' ? config : config.url;
        const requestConfig = typeof (config) !== 'string' ? Object.keys(config).filter(k => k !== 'url').reduce((resObj, k) => ({
            ...resObj, [k]: config[k]
        }), {}) : {};
        return httpVerbs
            .map(verb => verb.toLowerCase())
            .reduce((resObj, verb) => ({...resObj, [verb]: this.doRequest.bind(this, url, requestConfig)}), {});
    }

    doRequest = (url, requestConfig, extraConfig) => {
        axios.get(url, {});
        debugger;
    };
}

export default HttpAPI;