import {createContext} from 'react';
import RouteHandler from './handler';

const RouteContext = createContext(new RouteHandler({}));

export default RouteContext;