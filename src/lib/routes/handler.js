class Handler {
    routes = {};

    constructor (routes = {}) {
        this.routes = routes || {};
    }

    getRoute = (path = '') => (path || '').split('.').reduce((curObj, key) => curObj[key], this.routes);

    makeRoute = (path = '', params = {}) => {
        const route = this.getRoute(path);
        return this.buildRoute(route, params || {});
    };

    buildRoute (route = '', params = {}) {
        return Object.keys(params).reduce((r, k) => r.replace(new RegExp(`:${k}?{0,1}`, 'g'), params[k]), route);
    }
}

export default Handler;