import io from 'socket.io-client';

export const realtimeJoinAction = 'RT_JOIN';
export const realtimeJoinedAction = 'RT_JOINED';
export const realtimeJoinErrorAction = 'RT_JOIN_ERR';
export const realtimeLeaveAction = 'RT_LEAVE';
export const realtimeEmitAction = 'RT_EMIT';
export const realtimeEmittedAction = 'RT_EMITTED';
export const realtimeEmitErrorAction = 'RT_EMIT_ERR';
export const realtimeConnectAction = 'RT_CONNECTED';
export const realtimeDisconnectAction = 'RT_DISCONNECT';

const joinRoom = (config = {}, data = {}, roomList = []) => {
    const {io, store, joinedAction, joinErrorAction} = config;
    const dataObj = typeof (data) === 'string' ? {room: data} : data;
    const {room = '', id = ''} = dataObj;

    return new Promise((resolve, reject) => {
        if (!room) {
            reject('Cannot process a leave action dispatched without a room in its payload.');
        }

        if (!id) {
            reject('Cannot process a leave action dispatched without a subscriber id in its payload.');
        }

        const existingRoom = roomList.find(t => t.room = room);
        if (existingRoom) {
            // We need to add a new subscriber.
            const newRoomEntry = {...existingRoom, subscribers: [...existingRoom.subscribers, id]};
            store.dispatch({
                type   : joinedAction,
                payload: {
                    room
                }
            });
            resolve([...roomList.filter(t => t !== existingRoom), newRoomEntry]);
        } else {
            // We need to join the room.
            try {
                io.emit('join', room, (data) => {
                    store.dispatch({
                        type   : joinedAction,
                        payload: {
                            room,
                            data
                        }
                    });
                    resolve([...roomList, {room, subscribers: [id]}]);
                });
            } catch (err) {
                store.dispatch({
                    type   : joinErrorAction,
                    payload: {
                        room,
                        data
                    }
                });
                reject(`Error joining room: "${room}". Error: ${err}`);
            }
        }
    });
};

const leaveRoom = (config = {}, payload = {}, roomList = []) => {
    const {io} = config;
    const {room = '', id = ''} = payload;

    return new Promise((resolve, reject) => {
        if (!room) {
            reject('Cannot process a leave action dispatched without a room in its payload.');
        }

        if (!id) {
            reject('Cannot process a leave action dispatched without a subscriber id in its payload.');
        }

        const existingRoom = roomList.find(t => t.room = room);
        if (existingRoom) {
            if (existingRoom.subscribers.includes(id)) {
                const roomListWithoutCurrent = roomList.filter(r => r !== existingRoom);
                if (existingRoom.subscribers.length > 1) {
                    const firstIndex = existingRoom.subscribers.indexOf(id);
                    const newRoomEntry = {
                        ...existingRoom, subscribers: existingRoom.subscribers.filter((_, i) => i !== firstIndex)
                    };
                    resolve([...roomListWithoutCurrent, newRoomEntry]);
                } else {
                    try {
                        io.emit('leave', room, () => {
                            resolve([...roomListWithoutCurrent]);
                        });
                    } catch (err) {
                        reject(`Error leaving room: "${room}". Error: ${err}`);
                    }
                }
            } else {
                reject('The specified room was found, but the subscriber id is not valid.');
            }

            resolve();
        } else {
            reject('No subscription to the specified room was found.');
        }
    });
};

const emitMessageInRoom = (config = {}, payload = {}, roomList = []) => {
    const {io, store, emittedAction, emitErrorAction} = config;
    const {room = '', event = '', data = {}} = payload;

    return new Promise((resolve, reject) => {
        if (!room) {
            reject('An emit action was dispatched without a room in its payload.');
        }
        if (!event) {
            reject('An emit action was dispatched without a event (event name) in its payload.');
        }

        const existingRoom = roomList.find(t => t.room = room);
        if (existingRoom) {
            io.emit(event, data);
            store.dispatch({
                type   : emittedAction,
                payload: {
                    room,
                    event
                }
            });
            resolve();
        } else {
            store.dispatch({
                type   : emitErrorAction,
                payload: {
                    room,
                    event,
                    code: 'ROOM_NOT_JOINED'
                }
            });
            reject('An emit message referenced a room that had not been joined previously.');
        }
    });
};

export const reduxIOMiddleware = (config = {}) => {
    const {
              host             = 'http://localhost',
              options          = {},
              connectAction    = realtimeConnectAction,
              disconnectAction = realtimeDisconnectAction,
              emitAction       = realtimeEmitAction,
              emittedAction    = realtimeEmittedAction,
              emitErrorAction  = realtimeEmitErrorAction,
              joinAction       = realtimeJoinAction,
              joinedAction     = realtimeJoinedAction,
              joinErrorAction  = realtimeJoinErrorAction,
              leaveAction      = realtimeLeaveAction
          } = config;

    let subscribedRoomList = [];

    // Init socketIO
    const socket = io(host, options);

    const mwConfig = {
        connectAction,
        disconnectAction,
        emitAction,
        emittedAction,
        emitErrorAction,
        joinAction,
        joinedAction,
        joinErrorAction,
        leaveAction,
        socket
    };

    return store => {
        socket.on('connect', () => store.dispatch({type: mwConfig.connectAction}));
        socket.on('disconnect', () => store.dispatch({type: mwConfig.disconnectAction}));

        const _joinRoom = joinRoom.bind(null, mwConfig);
        const _emitMessageInRoom = emitMessageInRoom.bind(null, mwConfig);
        const _leaveRoom = leaveRoom.bind(null, mwConfig);

        return next => action => {
            const data = ((action || {}).payload);

            if (!data) {
                next(action);
                return false;
            }

            if (action.type === mwConfig.joinAction) {
                _joinRoom(data, subscribedRoomList).then((list) => subscribedRoomList = list).catch(err => console.info(`RT Middleware error: ${err}`));
            }

            if (action.type === mwConfig.emitAction) {
                _emitMessageInRoom(data, subscribedRoomList).catch(err => console.info(`RT Middleware error: ${err}`));
            }

            if (action.type === mwConfig.leaveAction) {
                _leaveRoom(data, subscribedRoomList).then((list) => subscribedRoomList = list).catch(err => console.info(`RT Middleware error: ${err}`));
            }

            // We never interrupt action processing.
            next(action);
        };
    };
};
